import Vue from "nativescript-vue";
import Vuex from "vuex";
import Login from "./components/Login";
import routes from "./router";
import store from "./store";
import RadSideDrawer from "nativescript-ui-sidedrawer/vue";
import CreditCardView from '@triniwiz/nativescript-stripe/vue';


Vue.use(CreditCardView);
Vue.registerElement('Carousel', () => require('nativescript-carousel').Carousel);
Vue.registerElement('CarouselItem', () => require('nativescript-carousel').CarouselItem);
Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer);
Vue.use(Vuex)

Vue.prototype.$routes = routes;
Vue.prototype.$store = store;

// Prints Vue logs when --env.production is *NOT* set while building
// Vue.config.silent = (TNS_ENV === 'production')

new Vue({
    store,
    render: (h) => h("frame", [h(Login)]),
}).$start();
