import Login from "../components/Login";
import Home from "../components/Home";
import DetailsHouse from "~/components/DetailsHouse";
import DetailsActivity from "~/components/DetailsActivity";
import Reservation from "~/components/Reservation";
import CreateReserve from "../components/CreateReserve";
import Logements from "../components/Logements";
import MyHouse from "../components/MyHouse";

const routes = {
  Login,
  Home,
  Reservation,
  DetailsHouse,
  DetailsActivity,
  CreateReserve,
  Logements,
  MyHouse,
}

export default routes
