import Vue from "vue";
import Vuex from "vuex";
import {Http} from "@nativescript/core";
import {Couchbase} from 'nativescript-couchbase-plugin';

const database = new Couchbase('atp_base');

Vue.use(Vuex);

const reserv = {
    state: {
        dates: {}
    },
    getters: {
        dates: (state) => state.dates,
    },
    actions: {
        setDates(context, dates) {
            context.commit("saveDates", dates);
        },
    },
    mutations: {
        saveDates(state, data) {
            state.dates = data;
        },
    },
}

const user = {
    state: {
        user: {},
        accessToken: "",
        refreshToken: "",
    },
    getters: {
        user: (state) => state.user,
        accessToken: (state) => state.accessToken,
        refreshToken: (state) => state.refreshToken,
    },
    actions: {
        async trySignin(context, loginForm) {
            const response = await Http.request({
                url: "https://api.atypikhouse.art/login",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                },
                content: JSON.stringify({
                    usr_mail: loginForm.emailInput,
                    usr_password: loginForm.passwordInput,
                }),
            })
            if (response.statusCode === 200) {
                var result = response.content.toJSON();
                context.commit("signinSuccess", result);
                return response
            } else {
                return response
            }
        },
        async refresh(context, refreshToken) {
            const response = await Http.request({
                url: "https://api.atypikhouse.art/refresh",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                },
                content: JSON.stringify({
                    usr_refresh_token: refreshToken,
                }),
            })
            if (response.statusCode === 201) {
                var result = response.content.toJSON();
                context.commit("refreshToken", result);
                return response
            } else {
                return response
            }
        },
    },
    mutations: {
        signinSuccess(state, data) {
            state.user = data;
            state.access_token = data.access_token;
            state.refresh_token = data.refresh_token;
        },
        refreshToken(state, data) {
            let db = database.query({select: []})
            state.user = db[0].userStore;
            state.access_token = data.access_token;
            state.refresh_token = data.refresh_token;
            state.user.refresh_token = data.refresh_token;
            state.user.access_token = data.access_token;
        },
    },
};

export default new Vuex.Store({
    modules: {
        user,
        reserv,
    },
});
